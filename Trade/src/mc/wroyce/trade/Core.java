package mc.wroyce.trade;

import mc.wroyce.trade.Manager.ChatManager;
import mc.wroyce.trade.Util.Trade;
import mc.wroyce.trade.Util.TradeUtil;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Core extends JavaPlugin implements Listener {

    public TradeUtil tradeutil;

    public void onEnable() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        if (!new File(getDataFolder(), "config.yml").exists()) {
            saveDefaultConfig();
        }

        this.tradeutil = new TradeUtil(this);

        Bukkit.getConsoleSender().sendMessage(ChatManager.toColor("&6(!) &eTrade &7has been &aEnabled"));
    }

    public void onDisable() {
        for (Trade trade : this.tradeutil.getAllTrades()) {
            trade.cancelTrade(true);
        }
    }

}
