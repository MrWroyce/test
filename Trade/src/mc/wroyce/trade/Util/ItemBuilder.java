package mc.wroyce.trade.Util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemBuilder {

    public static ItemStack getItem (Material material, int itemAmount, int itemData, String reset, String[] lores) {
        ItemStack item = new ItemStack (material, itemAmount, (byte) itemData);
        if (reset != null) {
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', reset));
            item.setItemMeta(meta);
        }

        if (lores != null) {
            List<String> lore = new ArrayList();
            String[] arrayOfString;
            int j = (arrayOfString = lores).length;
            for (int i = 0; i < j; i++) {
                String l = arrayOfString[i];
                lore.add(ChatColor.translateAlternateColorCodes('&', l));
            }
            ItemMeta meta = item.getItemMeta();
            meta.setLore(lore);
            item.setItemMeta(meta);
        }
        return item;
    }
}
